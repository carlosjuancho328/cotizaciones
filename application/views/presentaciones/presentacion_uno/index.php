<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="<?=base_url()?>dist/images/educacion.png">

    <title>Cotizaciones</title>

    <!-- Bootstrap core CSS -->
    <link href="<?=base_url()?>dist/bootstrap-3.3.6/css/bootstrap.min.css" rel="stylesheet">    
    <link href="<?=base_url()?>dist/bootstrap-social-gh-pages/assets/css/font-awesome.css" rel="stylesheet">
    <!-- <link href="<?=base_url()?>dist/bootstrap-social-gh-pages/assets/css/docs.css" rel="stylesheet" > -->
    <link href="<?=base_url()?>dist/bootstrap-social-gh-pages/bootstrap-social.css" rel="stylesheet" >
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="<?=base_url()?>dist/bootstrap-3.3.6/assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="<?=base_url()?>dist/bootstrap-3.3.6/assets/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- Custom styles for this template -->
    <link href="<?=base_url()?>dist/bootstrap-3.3.6/themes/carousel/carousel.css" rel="stylesheet">    
    <link href="<?=base_url()?>dist/css/estilos.css" rel="stylesheet">    
  </head>

  <body>
    <div class="navbar-wrapper">
      <div class="container">

        <nav class="navbar navbar-inverse navbar-static-top">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="#">
                <div class="col-sm-10">                                  
                    <img class="featurette-image img-responsive center-block" data-src="holder.js/170x35/auto" alt="Generic placeholder image">
                </div>
              </a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
              <ul class="nav navbar-nav">
                <li><a href="#">Inicio</a></li>
                <li><a href="#about">Servicios</a></li>
                <li><a href="#contact">Nosotros</a></li>
                <li><a href="#contact">Contactanos</a></li>                
                <li><a href="#contact">Ubicación</a></li>                
              </ul>
              <ul class="nav navbar-nav navbar-right">
                <li><a href="http://facebook.com" class="btn btn-social-icon btn-facebook">
          <i class="fa fa-facebook"></i></a></li>
                <li><a href="#contact" class="btn btn-social-icon btn-twitter"><i class="fa fa-twitter"></i></a></li>
                <li ><a href="#contact" class="btn btn-social-icon btn-instagram"><i class="fa fa-instagram"></i></a></li>
                <li ><a href="#contact" class="btn btn-social-icon btn-linkedin"><i class="fa fa-linkedin"></i></a></li>
              </ul>
            </div>
          </div>
        </nav>

      </div>
    </div>


    <!-- Carousel
    ================================================== -->
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>        
        <li data-target="#myCarousel" data-slide-to="1"></li>        
        <li data-target="#myCarousel" data-slide-to="2"></li>        
      </ol>
      <div class="carousel-inner" role="listbox">
        <div class="item active">
          <img class="first-slide" src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" alt="First slide">
          <div class="container">
            <div class="carousel-caption">
              <div class="row">                
                <div class="col-md-5">
                  <img class="featurette-image img-responsive center-block" data-src="holder.js/150x150/auto" alt="Generic placeholder image">
                </div>
                <h1>Artículo relevante.</h1>
                <p class="text-justify">XXXX XXX XXXXXXX XXXX XXX XXXXXX XXXXXXXXX X XXXXXXXXXXXX XXX XX XXXXXXXXXX XXXXXX XXX XXXXXXX XXX XX XXXX XXXXXXXX XXX XXXXXXXX XXXXXXXXX XX XXX XXXXXXXXX XXXXXXXXXXXXX XXXXXXXXXX XXXXXXXXXX XX XXXXXXX XXXXXXX XX XXXXXX XXXXXXXXXX XXXXXXXXXX XXXXXXXXXXX XXXXXXXXXXX XXX XXXXXXX XXXXXXXXX XX XXXX XXXXXX</p>

              </div>
            </div>
          </div>
        </div>        
      </div>      
      <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
    <div class="container marketing">

      <!-- Three columns of text below the carousel -->
      <div class="row">
      <h1 class="page-header text-center">Servicios</h1>
        <div class="col-lg-4">
          <img class="img-circle" src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" alt="Generic placeholder image" width="140" height="140">
          <h2>Servicio 1</h2>
          <p class="text-justify">XXXXX XXX XXXX XXXX XXXXX XXXXX XXX XXXXXXXXX XXXXX XXXXXX XXXXXXXX XXXXXX XX XXXXX XX XXXX XXXXXXXXX XXXXXXXX XX XX XXXXX XXXXX XXX XXXXXX XXXXX XX XXXXXXXXXXX XXX XXXXXXXXXX XX XXXXX XXXXXXXX XXXXXXX XXXXXX XXXXX.</p>
          <p><a class="btn btn-default" href="#" role="button">Ver detalles &raquo;</a></p>
        </div><!-- /.col-lg-4 -->
        <div class="col-lg-4">
          <img class="img-circle" src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" alt="Generic placeholder image" width="140" height="140">
          <h2>Servicio 2</h2>
          <p class="text-justify">XXXX XXXXXXX XXX XXX XXXXXXX XXXXXXX XXXX XXXX XXXXXXXXX XXXXXXX XXXX XXXXXXX XXXX XXX XXX XXXXX XXXX XXXXXX XXXXXXXXXXX XXXXX XXX XXXX XXXXXXXXXX XXXXX XXXXXXXX XXXXXX XX XXXXXX XXXXXXXX XXXXXX XXXXXX XXXXXXXXXXX XXXX.</p>
          <p><a class="btn btn-default" href="#" role="button">Ver detalles &raquo;</a></p>
        </div><!-- /.col-lg-4 -->
        <div class="col-lg-4">
          <img class="img-circle" src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" alt="Generic placeholder image" width="140" height="140">
          <h2>Servicio 3</h2>
          <p class="text-justify">XXXXX XXX XXXX XXXX XXXX XXXXX XXXXX XXXXXXX XX XXXXXXXXX XXX XXXXXXX XXXX XXXXX XXXXXXXXXX XX XXXXXX XXXXX XXXXX XXXXXXX XXXXXXX XXXXX XXXXXXXX XXXXXX XX XXXXXX XXXXXXXX XXXXXX XXXXXX XXXXXXXXXXX XXXXX XX XXXXXXXXX XXXXX XXXXX XXX XXXX XXXXX.</p>
          <p><a class="btn btn-default" href="#" role="button">Ver detalles &raquo;</a></p>
        </div><!-- /.col-lg-4 -->
      </div><!-- /.row -->


      <!-- START THE FEATURETTES -->

      <hr class="featurette-divider">

      <div class="row featurette">
        <div class="col-md-7">
          <h2 class="featurette-heading">Nosotros. <span class="text-muted"></span></h2>
          <p class="lead text-justify">XXXXX XXXXXXXXXXX XXXXX XXX XXXXX XXXXXX XXXXXXXXXX XXXXXXXXXX XX XXXXXX XXXXX XXXXX XXXXXXX XXXXXXX XXXXXXXX XXXXXXX XXXXXX XXXXXX XXX XXXXXXXXXXX XXXX XXXXXXXXXXXX XXXXX XXXXXXXX XXXXXX XX XXXXXX XXXXXXX.</p>
        </div>
        <div class="col-md-5">
          <img class="featurette-image img-responsive center-block" data-src="holder.js/500x500/auto" alt="Generic placeholder image">
        </div>
      </div>

      <!-- /END THE FEATURETTES -->
      <div class="container">
        <h2 class="text-center">Contactanos</h2>
        <div class="row justify-content-center">
          <div class="col-sm-8 col-md-offset-2 pb-5">
            <!--Form with header-->

            <form action="mail.php" method="post">
                <div class="card border-primary rounded-0">
                    
                    <div class="panel panel-default">
                      <div class="panel-heading bg-info text-white text-center">
                        <h3 class="panel-title"><h3><i class="glyphicon glyphicon-envelope"></i> </h3>
                            <p class="m-0">Con gusto te ayudaremos</p></h3>
                      </div>                      
                    </div>
                    <div class="card-body p-3">

                        <!--Body-->
                        <div class="input-group">
                          <span class="input-group-addon"><i class="glyphicon glyphicon-user text-info"></i></span>
                          <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre y Apellido" required>
                        </div>
                        <div class="input-group">
                          <span class="input-group-addon"><i class="glyphicon glyphicon-envelope text-info"></i></span>
                          <input type="email" class="form-control" id="nombre" name="email" placeholder="correo@xxxxx.com" required>
                        </div>
                        <div class="input-group">
                          <span class="input-group-addon"><i class="glyphicon glyphicon-comment text-info"></i></span>
                          <textarea class="form-control" placeholder="Envianos tu Mensaje" required></textarea>
                        </div>
                        <div class="text-center">
                            <input type="submit" value="Enviar" class="btn btn-info btn-block rounded-0 py-2">
                        </div>
                    </div>

                </div>
            </form>
            <!--Form with header-->
          </div>
        </div>
      </div>

      <div class="container">
      <h1 class="text-center">Ubicación</h1>
      <hr>
        <div class="row">
          <div class="col-sm-8">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d15256.888572885697!2d-96.74276098072434!3d17.06178332580589!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85c722135cd295d1%3A0x5cbad0ac56bccc7c!2sOaxaca%2C+Centro%2C+68000+Oaxaca%2C+Mexico!5e0!3m2!1sen!2sit!4v1532800895896" width="100%" height="320" frameborder="0" style="border:0" allowfullscreen></iframe>
          </div>

          <div class="col-sm-4" id="contact2">
              <h3>Dirección</h3>
              <hr align="left" width="50%">
              <h4 class="pt-2"></h4>
              <i class="glyphicon glyphicon-globe" style="color:#000"></i> XXXXXXX<br>
              <!-- <h4 class="pt-2">Contatti</h4> -->
              <!-- <i class="fas fa-phone" style="color:#000"></i> <a href="tel:+"> 123456 </a><br>
              <i class="fab fa-whatsapp" style="color:#000"></i><a href="tel:+"> 123456 </a><br>
              <h4 class="pt-2">Email</h4>
              <i class="fa fa-envelope" style="color:#000"></i> <a href="">test@test.com</a><br> -->
            </div>
        </div>
      </div>
      <div class="text-center">
                                            
          
          
                    
        </div>


      <!-- FOOTER -->
      <footer>
        <p class="pull-right"><a href="#">Back to top</a></p>
        <p>&copy; 2018 Lic. Juan Carlos Rojas Garcia <a href="#"></a>  <a href="#"></a></p>
      </footer>

    </div>
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <script src="<?=base_url();?>dist/jquery/js/jquery-1.10.2.js"></script>    
     <script>window.jQuery || document.write('<script src="<?=base_url()?>dist/bootstrap-3.3.6/assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="<?=base_url();?>dist/bootstrap-3.3.6/js/bootstrap.min.js"></script>   
    <!-- Just to make our placeholder images work. Don't actually copy the next line! -->
    <script src="<?=base_url()?>dist/bootstrap-3.3.6/assets/js/vendor/holder.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="<?=base_url()?>dist/bootstrap-3.3.6/assets/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
<?=$javascript?>
