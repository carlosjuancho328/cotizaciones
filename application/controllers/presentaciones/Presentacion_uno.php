<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Presentacion_uno extends CI_Controller {	
	function __construct(){
		parent::__construct();		
		$this->load->helper('presentaciones/my_xajax_presentacion_uno');		
	}
	public function index()
	{		
		$this->data["javascript"]=index();		
		$this->load->view('presentaciones/presentacion_uno/index',$this->data);
	}
}
