<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {	
	function __construct(){
		parent::__construct();		
		$this->load->helper('dashboard/my_xajax_dashboard');		
	}
	public function index()
	{		
		$this->data["javascript"]=index();		
		$this->load->view('dashboard/index',$this->data);
	}
}
