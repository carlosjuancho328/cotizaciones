<?php if ( ! defined('BASEPATH')) exit('No esta permitido el acceso directo');

if(!function_exists('index')){
	function index(){
		$javascript="
			<script type='text/javascript'>				
				$('.openPresentation').on('click',function(e){
					e.preventDefault();

					var presentacion=$(this).attr('data');
					console.log('".base_url()."presentaciones/'+presentacion);
					window.open('presentaciones/presentacion_uno','_top');
				});
			</script>
		";
		return $javascript;
	}
}